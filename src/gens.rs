use rand::Rng;
use crate::Range;
use uuid::Uuid;


pub fn gen_f64<R: Rng>(rng: &mut R, range: Range<f64>) -> f64 {
    if range.is_inclusive {
        rng.gen_range(range.start..=range.end)
    } else {
        rng.gen_range(range.start..range.end)
    }
}


pub fn gen_i64<R: Rng>(rng: &mut R, range: Range<i64>) -> i64 {
    if range.is_inclusive {
        rng.gen_range(range.start..=range.end)
    } else {
        rng.gen_range(range.start..range.end)
    }
}


pub fn gen_bool<R: Rng>(rng: &mut R, p: f64) -> bool {
    rng.gen_bool(p)
}

pub fn gen_uuid<R: Rng>(_rng: &mut R) -> Uuid {
    Uuid::new_v4()
}
