use crate::range::Range;
use crate::range_parsing::*;
use clap::{Args, Parser, Subcommand};
use std::fmt::Debug;

#[derive(Parser, Debug)]
#[clap(name = "rand")]
#[clap(author = "Vladislav Podporkin")]
#[clap(version)]
#[clap(about = "Utility for generating random values")]
#[clap(long_about = None)]
pub struct Cli {
    #[clap(subcommand)]
    pub cmd: Command,

    /// Count of random values
    #[clap(short, long, default_value = "1")]
    pub count: u64,

    /// Separator random values
    #[clap(short, long, default_value = " ")]
    pub separator: String,

    /// No print newline
    #[clap(long)]
    pub no_newline: bool,
}

#[derive(Subcommand, Debug)]
pub enum Command {
    /// Generate rational numbers
    #[clap(visible_alias = "d", visible_alias = "f")]
    Decimal(DecimalCommand),

    /// Generate integer numbers
    #[clap(visible_alias = "i")]
    Integer(IntegerCommand),

    /// Generate boolean.
    #[clap(visible_alias = "b")]
    Bool(BoolCommand),

    /// Generate UUID
    Uuid(UuidCommand),
}

#[derive(Args, Debug)]
pub struct DecimalCommand {
    /// Range of random values. use 'x..y' for [x, y] range or 'x._y' for [x, y) range.
    /// To escape a range of values (for example, when the range can include negative values), use '\'.
    /// Examples 'rand d 0..1', 'rand d 0.0..5.0', 'rand d \\-10..5', 'rand d \\-10..-5'.
    #[clap(parse(try_from_str=parse_range_f64_checked))]
    pub bounds: Range<f64>,

    /// Decimal places
    #[clap(short = 'p', long, default_value = "4", value_name = "VERSION")]
    pub decimal_places: u32,
}

#[derive(Args, Debug)]
pub struct IntegerCommand {
    /// Range of random values. use 'x..y' for [x, y] range or 'x._y' for [x, y) range.
    /// To escape a range of values (for example, when the range can include negative values), use '\'.
    /// Examples 'rand i 0..5', 'rand i \\-10..5', 'rand i \\-10..-5'.
    #[clap(parse(try_from_str=parse_range_i64_checked))]
    pub bounds: Range<i64>,
}

#[derive(Args, Debug)]
pub struct BoolCommand {
    /// Chance of getting a positive value
    #[clap(default_value = "0.5", value_name = "PROBABILITY")]
    pub probability: f64,

    /// The string to be printed when a positive value is get
    #[clap(short = 'y', long, default_value = "true", value_name = "ON_POS")]
    pub on_positive: String,

    /// The string to be printed when a negative value is get
    #[clap(short = 'n', long, default_value = "false", value_name = "ON_NEG")]
    pub on_negative: String,
}

impl Default for BoolCommand {
    fn default() -> Self {
        Self {
            probability: 0.5,
            on_positive: "true".to_string(),
            on_negative: "false".to_string(),
        }
    }
}

#[derive(Args, Debug)]
pub struct UuidCommand {
    /// UUID version
    #[clap(short = 'v', long, default_value = "4", value_name = "VERSION")]
    pub version: u32,
}
