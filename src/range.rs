use std::fmt::{Debug};

#[derive(Debug, Copy, Clone)]
pub struct Range<T> {
    pub start: T,
    pub end: T,
    pub is_inclusive: bool,
}
