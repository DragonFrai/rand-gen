use crate::args::{Cli, Command};
use crate::range::Range;
use clap::Parser;
use rand::rngs::SmallRng;
use rand::SeedableRng;
use std::fmt::{Display, Formatter};
use std::io::Write;

pub mod args;
pub mod gens;
pub mod range;
pub mod range_parsing;

fn main() {
    let args = Cli::parse();

    generate_and_print(&args);
}

fn print_iter<I, D>(delimiter: &str, print_nl: bool, values: I)
where
    D: Display,
    I: IntoIterator<Item = D>,
{
    let out = std::io::stdout();
    {
        let mut out = out.lock();
        let mut iter = values.into_iter();
        let first = iter.next();
        if let Some(first) = first {
            write!(&mut out, "{}", first).unwrap();
            while let Some(element) = iter.next() {
                write!(&mut out, "{}{}", delimiter, element).unwrap();
            }
        }
        if print_nl {
            writeln!(&mut out).unwrap();
        }
        out.flush().unwrap();
    }
}

struct F64WithPlaces {
    value: f64,
    places: u32,
}

impl Display for F64WithPlaces {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{value:.places$}",
            value = self.value,
            places = self.places as usize
        )
    }
}

fn generate_and_print(cli: &Cli) {
    let count = cli.count;
    let print_nl = !cli.no_newline;
    let delimiter = cli.separator.as_str();
    let mut rng = SmallRng::from_entropy();

    let cmd = &cli.cmd;

    match cmd {
        Command::Decimal(cmd) => {
            let range = cmd.bounds;
            let places = cmd.decimal_places;
            let nums = (0..count)
                .map(|_| gens::gen_f64(&mut rng, range))
                .map(|v| F64WithPlaces {
                    value: v,
                    places,
                });
            print_iter(delimiter, print_nl, nums);
        }
        Command::Integer(cmd) => {
            let range = cmd.bounds;
            let nums = (0..count).map(|_| gens::gen_i64(&mut rng, range));
            print_iter(delimiter, print_nl, nums);
        }
        Command::Bool(cmd) => {
            let chance = cmd.probability;
            let strs = (0..count).map(|_| gens::gen_bool(&mut rng, chance)).map(|v| {
                if v {
                    &cmd.on_positive
                } else {
                    &cmd.on_negative
                }
            });
            print_iter(delimiter, print_nl, strs);
        }
        Command::Uuid(cmd) => match cmd.version {
            4 => {
                let uuids = (0..count).map(|_| gens::gen_uuid(&mut rng));
                print_iter(delimiter, print_nl, uuids);
            }
            v => {
                eprintln!("Unsupported uuid version = {}", v)
            }
        },
    }
}
