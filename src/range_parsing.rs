use crate::Range;
use anyhow::anyhow;
use anyhow::Result;
use regex::Regex;
use std::fmt::{Debug, Display};
use std::str::FromStr;

#[rustfmt::skip]
pub fn parse_range_i64_checked(s: &str) -> Result<Range<i64>> {
    let s = s.trim_start_matches("\\");
    let range = parse_range(&s)?;
    let Range { start, end, is_inclusive } = range;
    anyhow::ensure!(
        is_inclusive && start <= end || !is_inclusive && start < end,
        "The start and end of the range cannot intersect"
    );
    Ok(range)
}

#[rustfmt::skip]
pub fn parse_range_f64_checked(s: &str) -> Result<Range<f64>> {
    let s = s.trim_start_matches("\\");
    let range = parse_range(&s)?;
    let Range { start, end, is_inclusive } = range;
    anyhow::ensure!(
        is_inclusive && start <= end || !is_inclusive && start < end,
        "The start and end of the range cannot intersect"
    );
    Ok(range)
}

pub const INCLUSIVE: &str = ".."; // [x, y]
pub const EXCLUSIVE: &str = "._"; // [x, y)

#[derive(Copy, Clone, Debug)]
pub enum RangeType {
    Inclusive,
    Exclusive,
}

fn bound_pair_from_str(s: &str) -> Result<RangeType, anyhow::Error> {
    match s {
        INCLUSIVE => Ok(RangeType::Inclusive),
        EXCLUSIVE => Ok(RangeType::Exclusive),
        _ => Err(anyhow!("Illegal range values separator")),
    }
}

fn _build_regex_str() -> String {
    let bound_exp = format!(
        "{ii}|{ie}",
        ii = regex::escape(INCLUSIVE),
        ie = regex::escape(EXCLUSIVE),
    );
    let value_exp = format!(".*");
    let regex_str = format!("^({value_exp})({bound_exp})({value_exp})$");
    regex_str
}

fn _build_regex() -> Regex {
    regex::Regex::new(&_build_regex_str()).unwrap()
}

const REG_STR: &str = r#"^(.*)(\.\.|\._)(.*)$"#;
fn range_regex() -> Regex {
    Regex::new(REG_STR).unwrap()
}

fn some_not_empty(s: &str) -> Option<&str> {
    if s.is_empty() {
        None
    } else {
        Some(s)
    }
}

pub fn parse_range<T, TE>(s: &str) -> Result<Range<T>>
where
    T: FromStr<Err = TE>,
    TE: Display + Display + Send + Sync + Into<anyhow::Error>,
{
    let regex = range_regex();
    let captures = regex.captures(s).ok_or_else(|| anyhow!("Illegal range"))?;

    let start_str = captures
        .get(1)
        .map(|m| m.as_str())
        .ok_or_else(|| anyhow!("Expected range start value"))?;
    let bound_str = captures
        .get(2)
        .and_then(|m| some_not_empty(m.as_str()))
        .ok_or_else(|| anyhow!("Expected range values separator"))?;
    let end_str = captures
        .get(3)
        .and_then(|m| some_not_empty(m.as_str()))
        .ok_or_else(|| anyhow!("Expected range end value"))?;

    let start = T::from_str(start_str).map_err(|e| anyhow!(e))?;
    let end = T::from_str(end_str).map_err(|e| anyhow!(e))?;
    let range_type = bound_pair_from_str(bound_str)?;

    let range = match range_type {
        RangeType::Inclusive => Range {
            start,
            end,
            is_inclusive: true,
        },
        RangeType::Exclusive => Range {
            start,
            end,
            is_inclusive: false,
        },
    };

    Ok(range)
}
